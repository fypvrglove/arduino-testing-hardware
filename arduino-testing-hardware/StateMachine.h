#ifndef STATE_MACHINE_H
#define STATE_MACHINE_H

#include "Arduino.h"
#include "PinLayout.h"

#include "UDPTest.h"
#include "MotionSensor.h"
#include "FlexSensor.h"
#include "HapticFeedback.h"
#include "SlaveComm.h"
#include "GripSimulation.h"

// States
#define TEST_WIFI_COMMUNICATION 1
#define TEST_MOTION_SENSOR 2
#define TEST_FLEX_SENSOR 3
#define TEST_HAPTIC_FEEDBACK 4
#define TEST_SLAVE_COMM 5
#define TEST_GRIP_SIMULATION 6

// Current State
#define STATE TEST_GRIP_SIMULATION

// I2C Pins for MotionSensor
#define SCL D1
#define SDA D2

// Global variables
extern MotionSensor motionSensor;
extern HapticFeedback hapticFeedback;
extern SlaveComm slaveComm;
extern FlexSensor flexSensor;
extern GripSimulation gripSimulation;

class StateMachine 
{
public:
	StateMachine();
	void Current_State();
private:
	void Test_WiFi_Communication();
	void Test_Magnetometer();
	void Test_Motion_Sensor();
	void Test_Flex_Sensor();
	void Test_Haptic_Feedback();
	void Test_Slave_Comm();
	void Test_Grip_Simulation();
};

#endif
