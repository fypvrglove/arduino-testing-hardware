#include "FlexSensor.h"

std::vector<int> sensors_read{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
std::vector<int> mux1_select{0, 0, 0};  // Bits = 2,1,0 (Mux 1 = CH0,CH1,CH2,CH3,CH4,CH5,CH6,CH7)
std::vector<int> mux2_select{0, 0, 0};  // Bits = X,1,0 (Mux 2 = CH0,CH1,CH2)

std::vector<int> bits000{0, 0, 0};
std::vector<int> bits001{0, 0, 1};
std::vector<int> bits010{0, 1, 0};
std::vector<int> bits011{0, 1, 1};
std::vector<int> bits100{1, 0, 0};
std::vector<int> bits101{1, 0, 1};
std::vector<int> bits110{1, 1, 0};
std::vector<int> bits111{1, 1, 1};

FlexSensor::FlexSensor()
{

}

void FlexSensor::Setup()
{
  pinMode(D1, OUTPUT);
  pinMode(D2, OUTPUT);
  pinMode(D3, OUTPUT);
  pinMode(D4, OUTPUT);
  pinMode(D5, OUTPUT);
  pinMode(D6, OUTPUT);

  MuxSelect(mux1_select, mux2_select);
  delay(100);
}

void FlexSensor::Test()
{
  // UDPSend((String)sensors_read[0]);
  
  // int serial_selection = Serial.read();

  // if (serial_selection != -1) {
  //   MuxTest(serial_selection);
  // } else {
  //   Serial.print("A0 = ");
  //   Serial.println(analogReadVal);
  // }
  
  if (mux1_select == bits000) {
    MuxSelect(mux1_select, mux2_select);
    delay(50);
    sensors_read[0] = analogRead(A0);
    Serial.printf("CH0 = %d ", sensors_read[0]);
    //mux1_select = {0, 0, 1};
  } else if (mux1_select == bits001) {
    MuxSelect(mux1_select, mux2_select);
    delay(50);
    sensors_read[1] = analogRead(A0);
    Serial.printf("CH1 = %d ", sensors_read[1]);
    mux1_select = {0, 1, 0};
  } else if (mux1_select == bits010) {
    MuxSelect(mux1_select, mux2_select);
    delay(50);
    sensors_read[2] = analogRead(A0);
    Serial.printf("CH2 = %d ", sensors_read[2]);
    mux1_select = {0, 1, 1};
  } else if (mux1_select == bits011) {
    MuxSelect(mux1_select, mux2_select);
    delay(50);
    sensors_read[3] = analogRead(A0);
    Serial.printf("CH3 = %d ", sensors_read[3]);
    mux1_select = {1, 0, 0};
  } else if (mux1_select == bits100) {
    MuxSelect(mux1_select, mux2_select);
    delay(50);
    sensors_read[4] = analogRead(A0);
    Serial.printf("CH4 = %d ", sensors_read[4]);
    mux1_select = {1, 0, 1};
  } else if (mux1_select == bits101) {
    MuxSelect(mux1_select, mux2_select);
    delay(50);
    sensors_read[5] = analogRead(A0);
    Serial.printf("CH5 = %d ", sensors_read[5]);
    mux1_select = {1, 1, 0};
  } else if (mux1_select == bits110) {
    MuxSelect(mux1_select, mux2_select);
    delay(50);
    sensors_read[6] = analogRead(A0);
    Serial.printf("CH6 = %d ", sensors_read[6]);
    mux1_select = {1, 1, 1};
  } else if (mux1_select == bits111) {
    if (mux2_select == bits000) {
      MuxSelect(mux1_select, mux2_select);
      delay(50);
      sensors_read[7] = analogRead(A0);
      Serial.printf("CH7 = %d ", sensors_read[7]);
      mux2_select = {0, 0, 1};
    } else if (mux2_select == bits001) {
      MuxSelect(mux1_select, mux2_select);
      delay(50);
      sensors_read[8] = analogRead(A0);
      Serial.printf("CH8 = %d ", sensors_read[8]);
      mux2_select = {0, 1, 0};
    } else if (mux2_select == bits010) {
      MuxSelect(mux1_select, mux2_select);
      delay(50);
      sensors_read[9] = analogRead(A0);
      Serial.printf("CH9 = %d \n", sensors_read[9]);
      mux1_select = {0, 0, 0};
      mux2_select = {0, 0, 0};
    } else {
      Serial.printf("Incorrect Mux1");
    }
  } else {
    Serial.printf("Incorrect Mux2");
  }
}

void FlexSensor::MuxSelect(std::vector<int> select1, std::vector<int> select2)
{
  if (select1 == bits000) {   // FFFF = 1111 1111 1111 1111 => GPIO15~GPIO0 to HIGH
    //           D5        D1 D2  D4  D3
    //           |         |  |   |   |
    // HIGH =>   0         0  0 0 0 0 0
    // LOW  => 0 1 000000001  1 0 1 0 1
    // GPIO_REG_WRITE(GPIO_OUT_W1TC_ADDRESS, 0x4035);   // Make these GPIO LOW

    // GPOS/GPOC is a faster way to digitalWrite where GPOS = HIGH and GPOC = LOW
    GPOS = (1 << D6);   // Active Low for Mux2 enable pin
    GPOC = (1 << D5);
    GPOC = (1 << D4);
    GPOC = (1 << D3);
    GPOC = (1 << D2);
    GPOC = (1 << D1);
    // digitalWrite(D5, LOW);
    // digitalWrite(D4, LOW);
    // digitalWrite(D3, LOW);
    // digitalWrite(D2, LOW);
    // digitalWrite(D1, LOW);
  } else if (select1 == bits001) {
    //           D5        D1 D2  D4  D3
    //           |         |  |   |   |
    // HIGH =>   0         1  0 0 0 0 0
    // LOW  => 0 1 000000000  1 0 1 0 1
    // GPIO_REG_WRITE(GPIO_OUT_W1TS_ADDRESS, 0x20);    // Make these GPIO HIGH
    // GPIO_REG_WRITE(GPIO_OUT_W1TC_ADDRESS, 0x4015);
    GPOS = (1 << D6);   // Active Low
    GPOC = (1 << D5);
    GPOC = (1 << D4);
    GPOC = (1 << D3);
    GPOC = (1 << D2);
    GPOS = (1 << D1);
    // digitalWrite(D5, LOW);
    // digitalWrite(D4, LOW);
    // digitalWrite(D3, LOW);
    // digitalWrite(D2, LOW);
    // digitalWrite(D1, HIGH);
  } else if (select1 == bits010) {
    //           D5        D1 D2  D4  D3
    //           |         |  |   |   |
    // HIGH =>   0         0  1 0 0 0 0
    // LOW  => 0 1 000000001  0 0 1 0 1
    // GPIO_REG_WRITE(GPIO_OUT_W1TS_ADDRESS, 0x10);
    // GPIO_REG_WRITE(GPIO_OUT_W1TC_ADDRESS, 0x4025);
    GPOS = (1 << D6);   // Active Low
    GPOC = (1 << D5);
    GPOC = (1 << D4);
    GPOC = (1 << D3);
    GPOS = (1 << D2);
    GPOC = (1 << D1);
    // digitalWrite(D5, LOW);
    // digitalWrite(D4, LOW);
    // digitalWrite(D3, LOW);
    // digitalWrite(D2, HIGH);
    // digitalWrite(D1, LOW);
  } else if (select1 == bits011) {
    //           D5        D1 D2  D4  D3
    //           |         |  |   |   |
    // HIGH =>   0         1  1 0 0 0 0
    // LOW  => 0 1 000000000  0 0 1 0 1
    // GPIO_REG_WRITE(GPIO_OUT_W1TS_ADDRESS, 0x30);
    // GPIO_REG_WRITE(GPIO_OUT_W1TC_ADDRESS, 0x4005);
    GPOS = (1 << D6);   // Active Low
    GPOC = (1 << D5);
    GPOC = (1 << D4);
    GPOC = (1 << D3);
    GPOS = (1 << D2);
    GPOS = (1 << D1);
    // digitalWrite(D5, LOW);
    // digitalWrite(D4, LOW);
    // digitalWrite(D3, LOW);
    // digitalWrite(D2, HIGH);
    // digitalWrite(D1, HIGH);
  } else if (select1 == bits100) {
    //           D5        D1 D2  D4  D3
    //           |         |  |   |   |
    // HIGH =>   0         0  0 0 0 0 1
    // LOW  => 0 1 000000001  1 0 1 0 0
    // GPIO_REG_WRITE(GPIO_OUT_W1TS_ADDRESS, 0x1);
    // GPIO_REG_WRITE(GPIO_OUT_W1TC_ADDRESS, 0x4034);
    GPOS = (1 << D6);   // Active Low
    GPOC = (1 << D5);
    GPOC = (1 << D4);
    GPOS = (1 << D3);
    GPOC = (1 << D2);
    GPOC = (1 << D1);
    // digitalWrite(D5, LOW);
    // digitalWrite(D4, LOW);
    // digitalWrite(D3, HIGH);
    // digitalWrite(D2, LOW);
    // digitalWrite(D1, LOW);
  } else if (select1 == bits101) {
    //           D5        D1 D2  D4  D3
    //           |         |  |   |   |
    // HIGH =>   0         1  0 0 0 0 1
    // LOW  => 0 1 000000000  1 0 1 0 0
    // GPIO_REG_WRITE(GPIO_OUT_W1TS_ADDRESS, 0x21);
    // GPIO_REG_WRITE(GPIO_OUT_W1TC_ADDRESS, 0x4014);
    GPOS = (1 << D6);   // Active Low
    GPOC = (1 << D5);
    GPOC = (1 << D4);
    GPOS = (1 << D3);
    GPOC = (1 << D2);
    GPOS = (1 << D1);
    // digitalWrite(D5, LOW);
    // digitalWrite(D4, LOW);
    // digitalWrite(D3, HIGH);
    // digitalWrite(D2, LOW);
    // digitalWrite(D1, HIGH);
  } else if (select1 == bits110) {
    //           D5        D1 D2  D4  D3
    //           |         |  |   |   |
    // HIGH =>   0         0  1 0 0 0 1
    // LOW  => 0 1 000000001  0 0 1 0 0
    // GPIO_REG_WRITE(GPIO_OUT_W1TS_ADDRESS, 0x11);
    // GPIO_REG_WRITE(GPIO_OUT_W1TC_ADDRESS, 0x4024);
    GPOS = (1 << D6);   // Active Low
    GPOC = (1 << D5);
    GPOC = (1 << D4);
    GPOS = (1 << D3);
    GPOS = (1 << D2);
    GPOC = (1 << D1);
    // digitalWrite(D5, LOW);
    // digitalWrite(D4, LOW);
    // digitalWrite(D3, HIGH);
    // digitalWrite(D2, HIGH);
    // digitalWrite(D1, LOW);
  } else if (select2 == bits000 && select1 == bits111) {
    //           D5        D1 D2  D4  D3
    //           |         |  |   |   |
    // HIGH =>   0         1  1 0 0 0 1
    // LOW  => 0 1 000000000  0 0 1 0 0
    // GPIO_REG_WRITE(GPIO_OUT_W1TS_ADDRESS, 0x31);
    // GPIO_REG_WRITE(GPIO_OUT_W1TC_ADDRESS, 0x4004);
    GPOC = (1 << D6);   // Active Low
    GPOC = (1 << D5);
    GPOC = (1 << D4);
    GPOS = (1 << D3);
    GPOS = (1 << D2);
    GPOS = (1 << D1);
    // digitalWrite(D5, LOW);
    // digitalWrite(D4, LOW);
    // digitalWrite(D3, HIGH);
    // digitalWrite(D2, HIGH);
    // digitalWrite(D1, HIGH);
  } else if (select2 == bits001 && select1 == bits111) {
    //           D5        D1 D2  D4  D3
    //           |         |  |   |   |
    // HIGH =>   0         1  1 0 1 0 1
    // LOW  => 0 1 000000000  0 0 0 0 0
    // GPIO_REG_WRITE(GPIO_OUT_W1TS_ADDRESS, 0x35);
    // GPIO_REG_WRITE(GPIO_OUT_W1TC_ADDRESS, 0x4000);
    GPOC = (1 << D6);   // Active Low
    GPOC = (1 << D5);
    GPOS = (1 << D4);
    GPOS = (1 << D3);
    GPOS = (1 << D2);
    GPOS = (1 << D1);
    // digitalWrite(D5, LOW);
    // digitalWrite(D4, HIGH);
    // digitalWrite(D3, HIGH);
    // digitalWrite(D2, HIGH);
    // digitalWrite(D1, HIGH);
  } else if (select2 == bits010 && select1 == bits111) {
    //           D5        D1 D2  D4  D3
    //           |         |  |   |   |
    // HIGH =>   1 000000001  1 0 0 0 1
    // LOW  => 0 0 000000000  0 0 1 0 0
    // GPIO_REG_WRITE(GPIO_OUT_W1TS_ADDRESS, 0x4031);
    // GPIO_REG_WRITE(GPIO_OUT_W1TC_ADDRESS, 0x4);
    GPOC = (1 << D6);   // Active Low
    GPOS = (1 << D5);
    GPOC = (1 << D4);
    GPOS = (1 << D3);
    GPOS = (1 << D2);
    GPOS = (1 << D1);
    // digitalWrite(D5, HIGH);
    // digitalWrite(D4, LOW);
    // digitalWrite(D3, HIGH);
    // digitalWrite(D2, HIGH);
    // digitalWrite(D1, HIGH);
  }

  //delay(50);
}


void FlexSensor::MuxTest(int selection) 
{
  if (selection == 48) {
    MuxSelect({0,0,0}, {0,0,0});
  } else if (selection == 49) {
    MuxSelect({0,0,1}, {0,0,0});
  } else if (selection == 50) {
    MuxSelect({0,1,0}, {0,0,0});
  } else if (selection == 51) {
    MuxSelect({0,1,1}, {0,0,0});
  } else if (selection == 52) {
    MuxSelect({1,0,0}, {0,0,0});
  } else if (selection == 53) {
    MuxSelect({1,0,1}, {0,0,0});
  } else if (selection == 54) {
    MuxSelect({1,1,0}, {0,0,0});
  } else if (selection == 55) {
    MuxSelect({1,1,1}, {0,0,0});
  } else if (selection == 56) {
    MuxSelect({1,1,1}, {0,0,1});
  } else if (selection == 57) {
    MuxSelect({1,1,1}, {0,1,0});
  } else {
    // Serial.printf("Incorrect Mux Selection\n");
    MuxSelect({0,0,0}, {0,0,0});
  }
}
