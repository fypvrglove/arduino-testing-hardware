#ifndef FLEX_SENSOR_H
#define FLEX_SENSOR_H

#include "General.h"
#include "UDPTest.h"

#include <vector>

//using namespace std;

class FlexSensor
{
  public:
    FlexSensor();
    void Setup();
    void Test();
  private:
    void MuxSelect(std::vector<int> select1, std::vector<int> select2);
    void MuxTest(int selection);
};

#endif
