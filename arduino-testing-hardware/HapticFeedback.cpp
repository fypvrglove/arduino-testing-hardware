#include "HapticFeedback.h"

HapticFeedback::HapticFeedback()
{

}

void HapticFeedback::Setup()
{
	// pinMode(HAPTIC_OUTPUT, OUTPUT);
	// pinMode(HAPTIC_MUX_CONTROL_0, OUTPUT);
	// pinMode(HAPTIC_MUX_CONTROL_1, OUTPUT);
	// pinMode(HAPTIC_MUX_CONTROL_2, OUTPUT);
	Serial.println("HapticFeedback Setup");
	// Serial.printf("HAPTIC_0 (D3) = %d\n", HAPTIC_0);
	// Serial.printf("HAPTIC_3 (RX) = %d\n", HAPTIC_3);
	// Serial.printf("SCL = %d\n", SCL);
	// Serial.printf("SDA = %d\n", SDA);
}

void HapticFeedback::Test()
{
	// if (hapticOutput == 0)
	// 	analogWrite(HAPTIC_OUTPUT, 1000);
	// else
	// 	analogWrite(HAPTIC_OUTPUT, 300);
	// if (hapticOutput & INDEX) SelectOuput(0);
	// if (hapticOutput & MIDDLE) SelectOuput(1);
	// if (hapticOutput & RING) SelectOuput(2);
	// if (hapticOutput & PINKY) SelectOuput(3);
	// if (hapticOutput & THUMB) SelectOuput(4);
}

void HapticFeedback::SelectOuput(int outputNumber)
{
	// Serial.println(outputNumber);
	if (outputNumber % 2)
		digitalWrite(HAPTIC_MUX_CONTROL_0, HIGH);
	else
		digitalWrite(HAPTIC_MUX_CONTROL_0, LOW);
	if (((int) outputNumber/2) % 2)
		digitalWrite(HAPTIC_MUX_CONTROL_1, HIGH);
	else
		digitalWrite(HAPTIC_MUX_CONTROL_1, LOW);
	if (((int) outputNumber/4) % 2)
		digitalWrite(HAPTIC_MUX_CONTROL_2, HIGH);
	else
		digitalWrite(HAPTIC_MUX_CONTROL_2, LOW);
}