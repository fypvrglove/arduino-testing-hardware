#ifndef HAPTIC_FEEDBACK_H
#define HAPTIC_FEEDBACK_H

#include "PinLayout.h"
#include "General.h"
#include <Wire.h>
#include <Ticker.h>

// Bit wise operations
#define INDEX 1
#define MIDDLE 2
#define RING 4
#define PINKY 8
#define THUMB 16

// Pins
#define HAPTIC_MUX_CONTROL_0 D5
#define HAPTIC_MUX_CONTROL_1 D6
#define HAPTIC_MUX_CONTROL_2 D7
#define HAPTIC_OUTPUT D8 // Goes to the Chip Enable Pin

extern int hapticOutput;

class HapticFeedback
{
  public:
    HapticFeedback();
    void Setup();
    void Test();
  private:
    void SelectOuput(int outputNumber);
};

#endif