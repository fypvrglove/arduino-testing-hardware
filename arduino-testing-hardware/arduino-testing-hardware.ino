#include "StateMachine.h"

StateMachine stateMachine;
MotionSensor motionSensor;
FlexSensor flexSensor;
HapticFeedback hapticFeedback;
SlaveComm slaveComm;
GripSimulation gripSimulation;

void setup()
{
	Serial.begin(115200);
	//Wire.begin(SDA, SCL);
	//slaveComm.Setup();
	//UDPSetup();
	//magnetometer.Setup();
	//motionSensor.Setup();
	//hapticFeedback.Setup();
	//flexSensor.Setup();
	gripSimulation.Setup();
	Serial.println("Setup Complete");
	
}

void loop()
{
	UDPLoop();
	stateMachine.Current_State();
}
