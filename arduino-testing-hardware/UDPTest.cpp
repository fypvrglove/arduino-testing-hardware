#include "UDPTest.h"

const char* ssid = "LAPTOP-5D1SHSVD 7717";
const char* password = "2122R|s1";
//const char* ssid = "DESKTOP-BCQ1O67 3578";
//const char* password = "97kG7-16";

WiFiUDP Udp;
String NodeIP = "";                 // for storing IP to send to PC/Phone
char incomingPacket[255];           // buffer for incoming packets

char charBuf[50] = "";

// Recevied Data
char latestPacket[255];
vector <string> separatedData;

// IPs and Ports
bool unityReady = true;
unsigned int localUdpPort = 4210;   // local port to listen for packets
unsigned int unityUdpPort = 51220;
IPAddress unityIp;
IPAddress localBroadcastIp;

// Haptic Feedback
int hapticOutput = 0;

void UDPSetup()
{
    pinMode(D6, OUTPUT);
    digitalWrite(D6, LOW);

    Serial.printf("Connecting to %s ", ssid);
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED)
    {
        delay(500);
        Serial.print(".");
    }
    Serial.println(" connected");

    Udp.begin(localUdpPort);
    NodeIP = WiFi.localIP().toString();
    Serial.printf("Now listening at IP %s, UDP port %d\n", NodeIP.c_str(), localUdpPort);

    localBroadcastIp = GetBroadcastAddress(WiFi.localIP());
}

void UDPSend(String packet)
{
    // Send data
    char charBuf[255];
    if (unityReady)
        Udp.beginPacket(unityIp, unityUdpPort);
    else
        Udp.beginPacket(localBroadcastIp, unityUdpPort);
    packet.toCharArray(charBuf, 255);
    Udp.write(charBuf);
    Udp.endPacket();
}

void UDPLoop()
{
    if (!unityReady)
    {
        UDPSend("Ready");
        delay(100);
    }
    else
    {
        // Start doing stuff
    }
    UDPReceive();
}

void UDPReceive()
{
    // Check for received data
    int packetSize = Udp.parsePacket();
    if (packetSize)
    {
        // Serial.printf("Received %d bytes from %s, port %d\n", packetSize, Udp.remoteIP().toString().c_str(), Udp.remotePort());
        int len = Udp.read(incomingPacket, 255);
        if (len > 0)
        {
          incomingPacket[len] = 0;
        }
        strcpy(latestPacket, incomingPacket);
        Serial.println(latestPacket);
        if (unityReady)
            ParseReceviedData();
        else
        {
            if (strcmp(latestPacket, "Ready") == 0)
            {
                unityUdpPort = Udp.remotePort();
                unityIp = Udp.remoteIP();
                unityReady = true;
                Serial.printf("Connected to IP=%s Port=%d\n", unityIp.toString().c_str(), unityUdpPort);
            }
        }
    }
}

void ParseReceviedData()
{
    char * pch;
    pch = strtok(latestPacket,":");
    while (pch != NULL)
    {
        separatedData.push_back((string) pch);
        pch = strtok(NULL, ":");
    }

    // for (int i=0; i < separatedData.size(); i++)
    // {
    //     Serial.println(separatedData[i].c_str());
    // }

    if (separatedData[0] == "H")
    {
        stringstream converter(separatedData[1]);
        converter >> hapticOutput;
    }
    
    separatedData.clear();
}

IPAddress GetBroadcastAddress(IPAddress ip)
{
    IPAddress broadcastIp;
    for (int i = 0; i < 3; i++)
    {
        broadcastIp[i] = ip[i];
    }
    broadcastIp[3] = 255;
    return broadcastIp;
}