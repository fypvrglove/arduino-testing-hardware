#include "SlaveComm.h"

#define SLAVE_ADDRESS 0x05 // Set in slave Wire.begin()
#define NO_OF_BEND_SENSORS 10

int bendData[NO_OF_BEND_SENSORS];

SlaveComm::SlaveComm()
{

}

void SlaveComm::Setup()
{
	SendReset();
	SendDebugMsg("SlaveComm Setup");
}

void SlaveComm::Test()
{
	GetUpdatedFlexSensorData();
	SendDebugMsg("SlaveComm Test");
	delay(1000);
}

void SlaveComm::SendDebugMsg(string debugMsg)
{
	SendString("Master: " + debugMsg + "\n");
}

void SlaveComm::SendString(string data)
{
	Wire.beginTransmission(SLAVE_ADDRESS);
	Wire.write(data.c_str());
	Wire.endTransmission();
}

string SlaveComm::GetData(int noOfBytes)
{
	string receivedData;
	Wire.requestFrom(SLAVE_ADDRESS, noOfBytes);
	while(Wire.available())
	{
		char c = Wire.read();
		receivedData.push_back(c);
	}
	return receivedData;
}

void SlaveComm::GetUpdatedFlexSensorData()
{
	SendString("BendData");
	for (int i = 0; i < NO_OF_BEND_SENSORS; i++)
	{
		string data = GetData(6);
		char * pch;
		char cstr[data.size() + 1];
		strcpy(cstr, data.c_str());
	    pch = strtok(cstr,":");
	    int j = 0;
	    int index;
	    int value;
	    while (pch != NULL)
	    {
	        if (j == 0)
	        {
	        	index = (int) pch[0] - '0';
	        }
	        else
	        {
	        	string str(pch);
	        	str.erase(0, min(str.find_first_not_of('0'), str.size()-1));
				std::istringstream(str) >> value;
	        }
	        j++;
	        pch = strtok(NULL, ":");
	    }
	    bendData[index] = value;
	}

	for (int i = 0; i < NO_OF_BEND_SENSORS; i++)
	{
		char msg[20];
	    sprintf(msg, "%d-%d", i, bendData[i]);
	    SendDebugMsg(msg);
	}
}

void SlaveComm::SendReset()
{
	SendString("Reset");
}