#ifndef GRIP_SIMULATION_H
#define GRIP_SIMULATION_H

#include "General.h"
#include "UDPTest.h"

#include <vector>

//using namespace std;

class GripSimulation
{
  public:
    GripSimulation();
    void Setup();
    void Test();
  private:
    void HBridgeSelect(std::vector<int> select1);
};

#endif
