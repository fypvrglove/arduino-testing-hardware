#include "StateMachine.h"

StateMachine::StateMachine()
{

}

void StateMachine::Current_State()
{
	#if STATE==TEST_WIFI_COMMUNICATION
		Test_WiFi_Communication();
	#endif
	#if STATE==TEST_MOTION_SENSOR
		Test_Motion_Sensor();
	#endif 
	#if STATE==TEST_FLEX_SENSOR
		Test_Flex_Sensor();
	#endif
	#if STATE==TEST_HAPTIC_FEEDBACK
		Test_Haptic_Feedback();
	#endif
	#if STATE==TEST_SLAVE_COMM
		Test_Slave_Comm();
	#endif
	#if STATE==TEST_GRIP_SIMULATION
		Test_Grip_Simulation();
	#endif 
}

void StateMachine::Test_WiFi_Communication()
{
	//UDPLoop();
	//HandleClient();
}

void StateMachine::Test_Motion_Sensor()
{
	motionSensor.Test();
}

void StateMachine::Test_Flex_Sensor()
{
	flexSensor.Test();
}

void StateMachine::Test_Haptic_Feedback()
{
	hapticFeedback.Test();
}

void StateMachine::Test_Slave_Comm()
{
	slaveComm.Test();
}
void StateMachine::Test_Grip_Simulation()
{
	gripSimulation.Test();
}
