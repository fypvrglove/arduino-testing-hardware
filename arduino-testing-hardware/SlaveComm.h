#ifndef SLAVE_COMM_H
#define SLAVE_COMM_H

#include "PinLayout.h"
#include "General.h"
#include <Wire.h>
#include <cstring>
#include <vector>
#include <string>
#include <sstream>

class SlaveComm
{
  public:
    SlaveComm();
    void Setup();
    void Test();

    void SendString(string data);
    void SendDebugMsg(string debugMsg);

    string GetData(int noOfBytes);
    void GetUpdatedFlexSensorData();
  private:
  	void SendReset();
};

#endif