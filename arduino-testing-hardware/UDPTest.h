#ifndef UDP_H
#define UDP_H

#include "HapticFeedback.h"
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <cstring>
#include <vector>
#include <string>
#include <sstream>

using namespace std;

void UDPSetup();
void UDPLoop();
void UDPSend(String packet);
void UDPReceive();
void ParseReceviedData();
IPAddress GetBroadcastAddress(IPAddress ip);

#endif
