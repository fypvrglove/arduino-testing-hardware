#ifndef PIN_LAYOUT_H
#define PIN_LAYOUT_H

#define CONFIGURATION 5

// #define SCL D1
// #define SDA D2

/*
	Configuration#1 - Particle Photon board available and ADCs arrive and they work
	Configuration#2 - Particle Photon board available and 2nd Arduino board
*/
#if CONFIGURATION==1 || CONFIGURATION==2

	// Bend Sensors use SCL and SDA pins

	// Grip Simulation
	#define GRIP_0 D4
	#define GRIP_1 D5
	#define GRIP_2 D6
	#define GRIP_3 D7
	#define GRIP_4 A0
	#define GRIP_5 A1
	#define GRIP_6 A2
	#define GRIP_7 A3
	#define GRIP_8 A4
	#define GRIP_9 A5

	// Haptic Feedback
	#define HAPTIC_0 D2
	#define HAPTIC_1 D3
	#define HAPTIC_2 WKP
	#define HAPTIC_3 RX
	#define HAPTIC_4 TX
	
#endif

// Particle Photon board available and 16 channel Mux arrives and works
#if CONFIGURATION==3
	
	// Bend Sensors
	#define BEND_0 A0
	#define BEND_1 A1
	#define BEND_2 A2
	#define BEND_3 A3
	#define BEND_4 A4
	#define BEND_5 A5
	#define BEND_6 WKP

	// Grip Simulation
	#define GRIP_0 D4
	#define GRIP_1 D5
	#define GRIP_2 D6
	#define GRIP_3 D7

	// Haptic Feedback
	#define HAPTIC_0 D2
	#define HAPTIC_1 D3
	#define HAPTIC_2 RX
	#define HAPTIC_3 TX
	#define HAPTIC_4 DAC

#endif

/*
	Configuration#4 - Particle Photon board NOT available and ADCs arrive and they work with Mux
	Configuration#5 - Particle Photon board NOT available and 2nd Arduino board with Mux
*/
#if CONFIGURATION==4 || CONFIGURATION==5
	
	// Bend Sensors use SCL and SDA pins
	
	// Grip Simulation
	#define GRIP_0 D0
	#define GRIP_1 D6
	#define GRIP_2 D7
	#define GRIP_3 D8

	// Haptic Feedback
	#define HAPTIC_0 D3
	#define HAPTIC_1 D4
	#define HAPTIC_2 D5
	#define HAPTIC_3 RX
	#define HAPTIC_4 TX
	
#endif

#endif