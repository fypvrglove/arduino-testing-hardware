#include "MasterComm.h"

MasterComm masterComm;

void setup()
{
	Serial.begin(115200);
	masterComm.Setup();
	Serial.println("Setup Complete");
}

void loop()
{
	masterComm.Test();
}
