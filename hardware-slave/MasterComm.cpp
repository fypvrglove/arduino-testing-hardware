#include "MasterComm.h"

// Bend data
bool bendDataRequested = false;
int bendData[NO_OF_BEND_SENSORS] = { };
int counter = 0;

void OnDataReceived(int howMany)
{
	char data[howMany];
	int i = 0;
	while(Wire.available())
	{
		char c = Wire.read();
		data[i] = c;
		i++;
	}
	data[howMany] = '\0';
	MasterComm::ParseReceivedData(data);
}

void OnDataRequested()
{
	if (bendDataRequested)
	{
		char str[6];
		sprintf(str, "%d:%04d", counter, bendData[counter]);
		Wire.write(str);
		if (counter < (NO_OF_BEND_SENSORS-1))
		{
			counter++;
		}
		else
		{
			counter = 0;
			bendDataRequested = false;
		}
	}
	else
	{
		Serial.println("i dont know what Master wants");
	}
}

MasterComm::MasterComm()
{

}

void MasterComm::Setup()
{
	Wire.begin(SLAVE_ADDRESS);
	Wire.onReceive(OnDataReceived);
	Wire.onRequest(OnDataRequested);
	bendData[0] = 2;
	bendData[1] = 4;
	bendData[2] = 8;
	bendData[3] = 16;
	bendData[4] = 32;
	bendData[5] = 64;
	bendData[6] = 128;
	bendData[7] = 256;
	bendData[8] = 512;
	bendData[9] = 1024;
	Serial.println("MasterComm Setup");
}

void MasterComm::Test()
{
	Serial.println("MasterComm Test");
	delay(1000);
}

static void MasterComm::ParseReceivedData(String data)
{
	if (data == "Reset")
	{
		Serial.println("\n\n\n\n\nSlave Reset By Master\n");
		// Do reset code
		counter = 0;
		bendDataRequested = false;
	}
	else if (data.startsWith("Master: "))
	{
		Serial.print(data);
	}
	else if (data == "BendData")
	{
		bendDataRequested = true;
	}
	else
	{
		Serial.print("dont know where this goes: ");
		Serial.print(data);
	}
}