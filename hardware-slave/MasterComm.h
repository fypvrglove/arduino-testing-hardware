#ifndef MASTER_COMM_H
#define MASTER_COMM_H

#include <Arduino.h>
#include <Wire.h>

#define NO_OF_BEND_SENSORS 10

#define SLAVE_ADDRESS 0x05

class MasterComm
{
  public:
    MasterComm();
    void Setup();
    void Test();

    static void ParseReceivedData(String data);
  private:

};

#endif